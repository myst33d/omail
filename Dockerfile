# Build image
FROM python:3-slim AS compile-image

# Copy sources to the container
WORKDIR /omail
COPY pyproject.toml .

ENV PATH=/root/.local/bin:$PATH

# Install dependencies
RUN pip install --user poetry
RUN poetry install


# Run image
FROM python:3-alpine AS run-image

# Copy Poetry and Poetry venv to the run image
COPY --from=compile-image /root/.local /root/.local
COPY --from=compile-image /root/.cache /root/.cache
ENV PATH=/root/.local/bin:$PATH

WORKDIR /omail
COPY . .

# Run omail
CMD ["poetry", "run", "python", "-m", "src"]
